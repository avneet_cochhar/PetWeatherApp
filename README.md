Pet Weather App is a node.js application which allows user to determine whether their pet needs to have an umbrella or not by looking at the current weather condition. 
They can add their pet's current location multiple time and use that information to determine weather forecast before planning any future trips with their pet.  



Pet Weather App Setup

1.Assuming  that you have a free Heroku account, any code editor. Make sure Node.js, git and npm installed locally or globally.

2. install the Heroku Command Line Interface (CLI)
Once installed, you can use the heroku command from your command shell.
    Example: 
    Heroku login
    
3. clone the sample application so that you have a local version of the code that you can then deploy to Heroku
    Example: 
    git clone https://github.com/heroku/node-js-getting-started.git

    cd node-js-getting-started
    
4.Copy/paste some file from this repository into your local project which you just cloned in the above step.
    Example: 
    Package.json, Package-lock, index.js, views folder
    
5.Deploy the App
    Run the following command:
    
    Npm install
    Npm install cool-ascii-faces
    Heroku create
    Git add .
    Git commit -m “init commit”
    Git push Heroku master
    Heroku ps:scale web=1
    Heroku local
    
    
source :https://devcenter.heroku.com/articles/getting-started-with-nodejs#introduction

If still find difficulty in setup, I have also included word document for better understanding.



